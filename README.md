# Machine learning example with Heart disease UCI dataset

A machine learning example project with Heart disease UCI dataset using Python, Scikit-learn, and Jupyter.

Includes:
- Data cleanup
- Feature correlation and sample distribution visualisations
- Feature selection with RFE
- Modeling with K-nn, Ridge and SVC
- Nested cross-validation
- Choosing a final model

## Usage

Read the *Report.pdf* or run *Project.ipynb* with Jupyter Lab.

## Requirements

- [Jupyter Lab](https://jupyter.org/)
- [Python 3](https://www.python.org/)
- [Scikit-learn](https://scikit-learn.org/stable/)
- [Pandas](https://pandas.pydata.org/) 
- [Numpy](https://numpy.org/)
- [Matplotlib](https://matplotlib.org/)
- [Seaborn](https://seaborn.pydata.org/)

## Licence

MIT licence