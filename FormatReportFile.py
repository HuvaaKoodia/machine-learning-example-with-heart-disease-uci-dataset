#Automatic formatting and cleanup of a report latex file.
import sys, os

filePath = sys.argv[1]
rows = []

with open(filePath, "r") as f:
	sectionI = 0
	for r in f:
		#Row modifications
		if "\\title" in r:
			rows.append("\\title{Machine learning example with Heart disease UCI dataset}")
			rows.append("\\author{HuvaaKoodia}")
			continue
		if "\\prompt{In}{incolor}{" in r:
			continue
		if "\\prompt{Out}{outcolor}{" in r:
			continue
		if "\\adjustimage{max size={0.9\linewidth}{0.9\paperheight}}{output_11_0.png}" in r:
			rows.append("\\adjustimage{max size={0.8\linewidth}{0.8\paperheight}}{output_11_0.png}")
			continue
		if "\\section" in r:
			sectionI += 1
			if sectionI > 1:
				rows.append("\pagebreak")

		rows.append(r)

outputFilePath = os.path.join(os.path.dirname(filePath), "Report.tex")
with open(outputFilePath, "w") as f:
	for r in rows:
		f.write(r)

print("Done")
